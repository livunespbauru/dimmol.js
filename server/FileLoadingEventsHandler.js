const LIVVCLIB = require('../lib/livvclib.server.min.js');

const MAXIMUM_SIZE = Math.pow(2, 20) * 10; // 10mb

function path(toFile) {
	return 'temp/' + toFile;
}

function FileData (name, size) {
	const parts = name.split('.');
	const nameItself = parts.slice(0, parts.length - 1).join('.');
	const extension = parts.slice(parts.length - 1);

	this.id = LIVVCLIB.RandomUtils.string(10);
	this.remoteName = name;
	this.localName = [nameItself, this.id, extension].join('.');
	this.size = size;
	this.uploaded = 0;
	this.data = '';
	this.fileHandler = null;
}

LIVVCLIB.ObjectEx.defineProperties(
	FileData.prototype,
	{
		complete: LIVVCLIB.ObjectEx.readOnly(function () {
			return this.size === this.uploaded;
		}),
		reachedLimit: LIVVCLIB.ObjectEx.readOnly(function () {
			return this.data > MAXIMUM_SIZE;
		})
	}
);

/**
 * @author luizssb
 */
function FileLoadingEventsHandler (fs) {
	LIVVCLIB.ClientEventsHandler.call(this);

	this._fs = fs;
	this._files = {};
}

const Prototype = FileLoadingEventsHandler.prototype = Object.assign(
	Object.create(LIVVCLIB.ClientEventsHandler.prototype),
	{ constructor: FileLoadingEventsHandler }
);

// Override
Prototype.handleNewClient = function (client) {
	const fs = this._fs;
	const files = this._files;

	client.on('dimmol:file:start', function (data, name, size) {
		const fileData = new FileData(name, size);
		files[fileData.id] = fileData;

		fs.open(path(fileData.localName), 'wx', 0755, function (error, handler) {
			if (error) {
				client.emit('dimmol:file:startError', name, error);
			} else {
				fileData.handler = handler;
				requestMore(fileData);
			}
		});
	})
		.on('dimmol:file:upload', function (data, fileId, uploadData) {
			const fileData = files[fileId];
			fileData.uploaded += uploadData.length;
			fileData.data += uploadData;

			if (fileData.complete) {
				saveToDisk(fileData, function () {
					client.emit(
						'dimmol:file:complete',
						fileData.remoteName, path(fileData.localName)
					);
				})
			} else if (fileData.data.length > MAXIMUM_SIZE) {
				saveToDisk(fileData, function () {
					fileData.data = ';';
					requestMore(fileData);
				});
			} else {
				requestMore(fileData);
			}
		});

	function saveToDisk (fileData, onSuccess) {
		fs.write(
			fileData.handler, fileData.data, null, 'Binary',
			function (error, written) {
				if (error) {
					client.emit(
						'dimmol:file:uploadError', fileData.remoteName, error
					);
				} else {
					onSuccess();
				}
			}
		);
	}

	function requestMore (fileData) {
		const dataIndex = fileData.uploaded;// / CHUNK_SIZE;
		const percent = 100 * fileData.uploaded / fileData.size;
		client.emit(
			'dimmol:file:more',
			fileData.remoteName, fileData.id, dataIndex, percent
		);
	}
};

module.exports = FileLoadingEventsHandler;
