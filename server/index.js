'use strict';

const os = require('os');
const nodeStatic = require('node-static');
const http = require('http');
const socketIO = require('socket.io');
const fs = require('fs');
const LIVVCLIB = require('../lib/livvclib.server.min.js');

const fileServer = new nodeStatic.Server();
const app = http.createServer(function (req, res) {
	fileServer.serve(req, res);
})
	.listen(8080);
const io = socketIO.listen(app);

const clientManager = new LIVVCLIB.SocketIOClientsManager(io);
const cluster = new LIVVCLIB.WebClusterSupport(clientManager)
	.addEventsHandler(
		new LIVVCLIB.ClusterEventsHandler(),
		new LIVVCLIB.NodeEventsHandler()
	);
cluster.setup();

if (supportsVRPN()) {
	const lib = require('nbind').init().lib;
	const factory = new LIVVCLIB.DefaultInterfacesFactory(lib);
	cluster.addEventsHandler(new LIVVCLIB.DeviceEventsHandler(factory))
}

function supportsVRPN () {
	return /^win/.test(process.platform);
}

const FileLoadingEventsHandler = require('./FileLoadingEventsHandler');
cluster.addEventsHandler(new FileLoadingEventsHandler(fs));