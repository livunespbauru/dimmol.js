function eventName (name) {
	return 'dimmol:' + name;
}
const Events = {
	LOAD_FILE: 'dimmol:loadFile',
	REMOVE_COMPONENT: 'dimmol:removeComponent'
};

const ComponentEvents = {
	ANNOTATIONS: 'dimmol:component:annotations',
	ADD_REPRESENTATION: 'dimmol:component:addRepresentation',
	REMOVE_REPRESENTATION: 'dimmol:component:removeRepresentation'
};

/**
 * @author luizssb
 */
function DimmolControl (stage) {
	const KEY_SYNC = 'dimmol:stage:groups';

	this.stage = stage;
	this.clusterManager = new ClusterManager()
		.onJoinCluster.register('', function () {
			stage.removeAllComponents();

			this.devicesControl = new DevicesControl(this.clusterManager);
			this.moleculeControl =
				new MoleculeControl(stage, this.clusterManager);
			this.stageSync =
				new LIVVCLIB.Sync(this.clusterManager.clusterConn, KEY_SYNC);

			this._registerTrackings(stage, this.stageSync);
			this._registerToClusterEvents(this.clusterManager.eventManager);
		}.bind(this))
		.onLeaveCluster.register('', function () {
			stage.removeAllComponents();
		});

	this.signals = {
		busy: new signals.Signal(),
		uploadProgress: new signals.Signal(),
		notBusy: new signals.Signal(),
		uploadFailure: new signals.Signal()
	};
}
DimmolControl.prototype._registerTrackings = function (stage, sync) {
	sync
		.trackAllAttributes(
			stage.viewer.translationGroup.position,
			function (obj, attr, newValue) {
				obj[attr] =	 newValue;
				stage.viewer.requestRender();
			}
		)
		.trackAllAttributes(
			stage.viewer.rotationGroup.rotation,
			function (obj, attr, newValue) {
				LIVVCLIB.RotationSyncSetter(obj, attr, newValue);
				stage.viewer.requestRender();
			}
		)
		.trackMethod(stage, 'setParameters');
};
DimmolControl.prototype._registerToClusterEvents = function (eventManager) {
	eventManager
		.on(Events.LOAD_FILE, this.loadFile.bind(this))
		.on(Events.REMOVE_COMPONENT, function (componentIndex) {
			if (componentIndex) {
				this.stage.removeComponent(this.stage.compList[componentIndex]);
			} else {
				this.stage.removeAllComponents();
			}
		})
		.on(ComponentEvents.ANNOTATIONS, function (data) {
			this.setAnnotationsVisible(
				this.stage.compList[data.componentIndex], data.visible
			);
		})
		.on(
			ComponentEvents.ADD_REPRESENTATION, function (data) {
				this.addRepresentation(
					this.stage.compList[data.componentIndex],
					data.name, data.params
				);
			}.bind(this)
		)
		.on(
			ComponentEvents.REMOVE_REPRESENTATION, function (data) {
				this.removeRepresentation(
					this.stage.compList[data.componentIndex]
						.reprList[data.reprIndex]
				);
			}.bind(this)
		);
};
DimmolControl.prototype.loadFile = function (file) {
	const clusterManager = this.clusterManager;
	const moleculeControl = this.moleculeControl;
	const signals = this.signals;

	if (!clusterManager.connected) {
		return this.stage.loadFile(file, { defaultRepresentation: true });
	}

	// return this.clusterManager.doOnNode(
	// 	function () { // Master
			const fileName = typeof file === 'string' ? file : file.name;

			if (fileName.indexOf('rcsb://') === 0 ||
				fileName.indexOf('http') === 0) {
				clusterManager.eventManager.emit(Events.LOAD_FILE, file);
				return moleculeControl.load(file);
			} else {
				signals.busy.dispatch();

				return uploadFile(file, clusterManager, function (percent) {
					signals.uploadProgress.dispatch(percent);
				})
					.then(function (fileURL) {
						signals.notBusy.dispatch();

						const fileURI =
							clusterManager.serverConn.serverAddress + '/' +
							fileURL;
						clusterManager.eventManager
							.emit(Events.LOAD_FILE, fileURI);
						return moleculeControl.load(file);
					})
					.catch(function (error) {
						signals.notBusy.dispatch();
						console.log(error);
					});
			}
		// },
		// function () { // Slave
		// 	return new Promise(function (resolve, reject) {
		// 		reject(new Error('slave node can\'t load files'));
		// 	});
		// }
	// );
};
DimmolControl.prototype.removeComponent = function (component) {
	if (!this.clusterManager.connected) {
		return this.stage.removeComponent(component);
	}

	this.clusterManager.eventManager.emit(
		Events.Component.REMOVE, this.stage.compList.indexOf(component)
	);
	return this.moleculeControl.removeComponent(component);
};
DimmolControl.prototype.removeAllComponents = function () {
	if (!this.clusterManager.connected) {
		return this.stage.removeAllComponents();
	}

	this.clusterManager.eventManager.emit(Events.Component.REMOVE);
	return this.moleculeControl.removeAllComponents();
};
DimmolControl.prototype.setAnnotationsVisible = function (component, visible) {
	if (!this.clusterManager.connected) {
		return component.annotationList.forEach(function (annotation) {
			annotation.setVisibility(visible);
		});
	}

	this.clusterManager.eventManager.emit(
		ComponentEvents.ANNOTATIONS,
		{
			componentIndex: this.stage.compList.indexOf(component),
			visible: visible
		}
	);
	return this.moleculeControl.setAnnotationsVisible(component, visible);
};
DimmolControl.prototype.addRepresentation = function (
	toComponent, reprName, params
) {
	if (!this.clusterManager.connected) {
		return component.addRepresentation(reprName);
	}

	this.clusterManager.eventManager.emit(
		ComponentEvents.ADD_REPRESENTATION,
		{
			componentIndex: this.stage.compList.indexOf(toComponent),
			name: reprName,
			params: params
		}
	);
	return this.moleculeControl.addRepresentation(toComponent, reprName, params);
};
DimmolControl.prototype.removeRepresentation = function (repr) {
	if (!this.clusterManager.connected) {
		return repr.parent.removeRepresentation(repr);
	}

	this.clusterManager.eventManager.emit(
		ComponentEvents.REMOVE_REPRESENTATION,
		{
			componentIndex: this.stage.compList.indexOf(repr.parent),
			reprIndex: repr.parent.reprList.indexOf(repr)
		}
	);
	return this.moleculeControl.removeRepresentation(repr);
};

function uploadFile (file, clusterManager, onProgress) {
	const CHUNK_SIZE = Math.pow(2, 19); // 512kb

	return new Promise(function (resolve, reject) {
		const fileReader = new FileReader();
		const localFileName = file.name.replace('\\', '/').split('/').pop();
		let remoteFileId;

		fileReader.onload = function (event) {
			clusterManager.serverConn
				.emit('dimmol:file:upload', remoteFileId, event.target.result);
		};
		clusterManager.serverConn
			.on('dimmol:file:startError', function (data, fileName, error) {
				removeListeners();
				reject(error);
			})
			.on(
				'dimmol:file:more',
				function (data, fileName, fileId, index, percent) {
					(onProgress || function () {})(percent);
					// var dataIndex = index * CHUNK_SIZE; 
					const sliceMethod = file.slice ? file.slice
						: (file.webkitSlice ? file.webkitSlice : file.mozSlice);

					const fileSlice = sliceMethod.apply(
						file,
						[
							index,
							index + Math.min(CHUNK_SIZE, file.size - index)
						]
					);
					remoteFileId = fileId;
					fileReader.readAsBinaryString(fileSlice);
				}
			)
			.on('dimmol:file:uploadError', function (data, fileName, error) {
				removeListeners();
				reject(error);
			})
			.on('dimmol:file:complete', function (data, fileName, remoteName) {
				removeListeners();
				resolve(remoteName);
			})
			.emit('dimmol:file:start', file.name, file.size);
	});

	function removeListeners () {
		clusterManager.serverConn
			.off('dimmol:file:startError')
			.off('dimmol:file:more')
			.off('dimmol:file:uploadError')
			.off('dimmol:file:complete');
	}
}
