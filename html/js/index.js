const SERVER_IP = '192.168.0.106';
const SERVER_PORT = '8080';
const SERVER_ADDRESS = SERVER_IP + ':' + SERVER_PORT;
const ROOM = 'foobar';

const DEVICE_NAME = 'XInput0'; 

const _clusterManager = new ClusterManager(SERVER_ADDRESS, ROOM);
const _devicesController = new DevicesControl(_clusterManager);

function connectToCluster () {
	return _clusterManager.connect().then(function () {
		document.getElementById('connectCluster').disabled = true;
	})
	.catch(function (error) {
		alert(`Não conseguiu conectar.\nErro: ${error.message}`);;
	});
}


