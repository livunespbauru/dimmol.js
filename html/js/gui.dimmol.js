NGL.Preferences.prototype.dimmol_getKey = function (key, defaultValue) {
	const value = this.getKey('dimmol_' + key);
	return value === undefined ? defaultValue : value;
};
NGL.Preferences.prototype.dimmol_setKey = function () {
	if (typeof arguments[0] === 'object' && arguments.length === 1) {
		for (let key in arguments[0]) {
			this.setKey('dimmol_' + key, arguments[0][key]);
		}
	} else {
		this.setKey('dimmol_' + arguments[0], arguments[1]);	
	}
};

function getErrorMessage (error) {
	return error.message || JSON.stringify(error);
}

NGL.setupUIBlock = function (stage) {
	const block = new NGL.UIBlockWidget(stage);
	document.body.appendChild(block.dom);

	const signals = stage.dimmol.signals;
	let progress;
	signals.busy.add(function () {
		progress = block.show('Preparing file for synchronization');
	});
	signals.uploadProgress.add(function (percent) {
		progress.updateStatus();
	});
	signals.notBusy.add(function () {
		block.hide();
	});
};
NGL.UIBlockWidget = function (stage) {
	UI.OverlayPanel.call(this);

	this.content = new NGL.UIBlockContentWidget(stage);

	this.setWidth('100%')
		.setHeight('100%')
		.setBackgroundColor('#0007')
		.setZIndex(2e9)
		.setDisplay('none')
		.add(this.content);
};
NGL.UIBlockWidget.prototype = Object.assign(
	Object.create(UI.OverlayPanel.prototype),
	{ constructor: NGL.UIBlockWidget }
);
NGL.UIBlockWidget.prototype.show = function (message) {
	this.setDisplay('block');
	return this.content
		.setMessage(message)
		.refresh();
};
NGL.UIBlockWidget.prototype.hide = function () {
	this.setDisplay('none');
};

NGL.MenubarDimmolWidget = function (stage, preferences) {
	return UI.MenubarHelper.createMenuContainer(
		'Dimmol',
		UI.MenubarHelper.createOptionsPanel([
			UI.MenubarHelper.createOption(
				'Cluster options',
				onOptionClick(
					makeWidget(NGL.ClusterWidget)
						.setDisplay('block')
						.setWidgetPosition(10, 50)
				)
			),
			UI.MenubarHelper.createOption(
				'Screen options', onOptionClick(makeWidget(NGL.ScreenWidget))
			),
			UI.MenubarHelper.createOption(
				'Devices', onOptionClick(makeWidget(NGL.DevicesWidget))
			)
		])
	);

	function makeWidget (widgetType) {
		return new widgetType(stage, preferences)
			.setDisplay('none')
			.attach();
	}

	function onOptionClick (widget) {
		return function (e) {
			widget.setOpacity('0.9')
				.setDisplay('block')
				.setWidgetPosition( 50, 50 )
		};
	}
};

NGL.BaseDimmolWidget = function (title, stage, preferences, dismissable) {
	UI.OverlayPanel.call(this);
	this.stage = stage;
	this.preferences = preferences;

	this.xOffset = 0
	this.yOffset = 0

	this.prevX = 0
	this.prevY = 0

	this.headingPanel = new UI.Panel()
		.setBorderBottom('1px solid #555')
		.setHeight('25px')
		.add(new UI.Text(title).setFontStyle('italic'));

	if (dismissable || dismissable === undefined) {
		this.headingPanel.add(
			new UI.Icon('times')
				.setCursor('pointer')
				.setMarginLeft('20px')
				.setFloat('right')
				.onClick(function () {
					this.setDisplay('none')
				}.bind(this))
		)
		.setCursor('move')
		.onMouseDown(function (e) {
			const mousemove = this.onMousemove.bind(this)

			if (e.which === 1) {
				document.addEventListener('mousemove', mousemove)
			}
			document.addEventListener('mouseup', function (e) {
				document.removeEventListener('mousemove', mousemove)
			})
		}.bind(this))
	}

	this.listingPanel = new UI.Panel()
		.setMarginTop('10px')
		.setMinHeight('100px')
		.setMaxHeight('500px')
		.setMaxWidth('600px')
		.setOverflow('auto')

	this.setup(this.listingPanel, stage, preferences);
	this.add(this.headingPanel, this.listingPanel)
};
NGL.BaseDimmolWidget.prototype = Object.assign(
	Object.create(UI.OverlayPanel.prototype),
	{ constructor: NGL.BaseDimmolWidget }
);
NGL.BaseDimmolWidget.prototype.setWidgetPosition = function (left, top) {
	this.xOffset = left
	this.yOffset = top
	this.prevX = 0
	this.prevY = 0
	this.dom.style.top = this.yOffset + 'px'
	this.dom.style.left = this.xOffset + 'px'
	return this;
};
NGL.BaseDimmolWidget.prototype.onMousemove = function (e) {
	if (this.prevX === 0) {
		this.prevX = e.clientX
		this.prevY = e.clientY
	}
	this.xOffset -= this.prevX - e.clientX
	this.yOffset -= this.prevY - e.clientY
	this.prevX = e.clientX
	this.prevY = e.clientY
	this.dom.style.top = this.yOffset + 'px'
	this.dom.style.left = this.xOffset + 'px'
};
NGL.BaseDimmolWidget.prototype.refresh = function () {
	this.listingPanel.clear();
	this.setup(this.listingPanel, this.stage, this.preferences);
	return this;
};
NGL.BaseDimmolWidget.prototype.setup = LIVVCLIB.ObjectEx.abstractMethod;

NGL.UIBlockContentWidget = function (stage) {
	this.message = '';
	this.currentStage = 0;
	NGL.BaseDimmolWidget.call(this, 'Running...', stage, null, false);
};
NGL.UIBlockContentWidget.prototype = Object.assign(
	Object.create(NGL.BaseDimmolWidget.prototype),
	{ constructor: NGL.UIBlockContentWidget }
);
NGL.UIBlockContentWidget.prototype.setup = function (forPanel, stage, preferences) {
	forPanel.add(new UI.Text(this.message));

	for (let idx = 0; idx < this.currentStage; ++idx) {
		forPanel.add(new UI.Text('.'));
	}
};
NGL.UIBlockContentWidget.prototype.updateStatus = function () {
	this.currentStage = (this.currentStage + 1) % 4;
	return NGL.BaseDimmolWidget.prototype.refresh.call(this);
};
NGL.UIBlockContentWidget.prototype.setMessage = function (message) {
	this.message = message;
	return this;
};
NGL.UIBlockContentWidget.prototype.refresh = function () {
	this.currentStage = 0;
	return NGL.BaseDimmolWidget.prototype.refresh.call(this);
};

NGL.ClusterWidget = function (stage, preferences) {
	NGL.BaseDimmolWidget.call(this, 'Cluster settings', stage, preferences)
};
NGL.ClusterWidget.prototype = Object.assign(
	Object.create(NGL.BaseDimmolWidget.prototype),
	{ constructor: NGL.ClusterWidget }
);
NGL.ClusterWidget.prototype.setup = function (forPanel, stage, preferences) {
	const serverAddress =
		preferences.dimmol_getKey('serverAddress', window.location.origin);
	const room = 
		preferences.dimmol_getKey('room', LIVVCLIB.RandomUtils.string(4));

	const textServer = new UI.Input(serverAddress);
	const buttonServer = new UI.Button('Reset').onClick(function () {
		textServer.setValue(window.location.origin);
	});
	const textRoom = new UI.Input(room);
	const buttonRoom = new UI.Button('Random').onClick(function () {
		textRoom.setValue(LIVVCLIB.RandomUtils.string(4));
	});
	const buttonConnect = new UI.Button('Connect').onClick(connect);
	const buttonDisconnect = new UI.Button('Disconnect').onClick(disconnect)
		.setDisabled(true);
	const textStatus = new UI.Text('not connected');

	forPanel.add(
		new UI.Html('Status:&nbsp;'), textStatus, new UI.Break()
	);
	[
		['Server address', textServer, buttonServer],
		['Room', textRoom, buttonRoom]
	].forEach(function (element) {
		forPanel.add(
			new UI.Text(element[0]).setWidth('120px'),
			element[1],
			element[2],
			new UI.Break()
		);
	});
	forPanel.add(buttonConnect, buttonDisconnect);

	function connect () {
		textServer.setDisabled(true);
		buttonServer.setDisabled(true);
		textRoom.setDisabled(true);
		buttonRoom.setDisabled(true)
		buttonConnect.setDisabled(true);

		stage.dimmol.clusterManager
			.onLeaveCluster.register(LIVVCLIB.RandomUtils.string(4), function () {
				disconnect();
				return false;
			})
			.connect(textServer.getValue(), textRoom.getValue())
			.then(function () {
				preferences.dimmol_setKey({
					'serverAddress': textServer.getValue(),
					'room': textRoom.getValue()
				});
				buttonDisconnect.setDisabled(false);
				textStatus.setValue(LIVVCLIB.ObjectEx.enumToString(
					LIVVCLIB.NodeType,
					stage.dimmol.clusterManager.clusterConn.nodeType
				));
			})
			.catch(function (e) {
                alert(`Failed to connect to cluster.\n"${getErrorMessage(e)}"`);
				textServer.setDisabled(false);
				buttonServer.setDisabled(false);
				textRoom.setDisabled(false);
				buttonRoom.setDisabled(false);
				buttonConnect.setDisabled(false);
			})
	}

	function disconnect () {
		stage.dimmol.clusterManager.disconnect();
		buttonDisconnect.setDisabled(true);

		textStatus.setValue('not connected');

		textServer.setDisabled(false);
		buttonServer.setDisabled(false);
		textRoom.setDisabled(false);
		buttonRoom.setDisabled(false);
		buttonConnect.setDisabled(false);
	}
};

NGL.ScreenWidget = function (stage, preferences) {
	NGL.BaseDimmolWidget.call(this, 'Screen settings', stage, preferences)
};
NGL.ScreenWidget.prototype = Object.assign(
	Object.create(NGL.BaseDimmolWidget.prototype),
	{ constructor: NGL.ScreenWidget }
);
NGL.ScreenWidget.prototype.setup = function (forPanel, stage, preferences) {
	const screen = preferences.dimmol_getKey('screen', new LIVVCLIB.Screen());
	const points = ['pA', 'pB', 'pC', 'pE'];
	const components = ['x', 'y', 'z'];

	points.forEach(function (point) {
		components.forEach(function (component) {
			const id = point + '.' + component;
			forPanel.add(
				new UI.Text(id)
					.setWidth('40px')
					.setMarginLeft('5px'),
				new UI.Input(screen[point][component])
					.setWidth('50px')
					.setId(id)
			);
		});
		forPanel.add(new UI.Break());
	});

	forPanel.add(
		new UI.Break(),
		new UI.Button('Set screen points').onClick(function (e) {
			const newScreen = new LIVVCLIB.Screen();
			points.forEach(function (point) {
				components.forEach(function (component) {
					const id = point + '.' + component;
					newScreen[point][screen] = parseInt(
						document.getElementById(id).value
					);
				})
			});

			stage.viewer.perspectiveCamera.screen = newScreen;
			preferences.dimmol_setKey('screen', newScreen);		
		}),
		new UI.Button('Reset screen').onClick(function (e) {
			stage.viewer.perspectiveCamera.screen = null;
		})
	);
};

NGL.DevicesWidget = function (stage, preferences) {
	NGL.BaseDimmolWidget.call(this, 'Devices', stage, preferences);

	const tag = LIVVCLIB.RandomUtils.string(4);
	stage.dimmol.clusterManager
		.onJoinCluster.register(tag, this.refresh.bind(this))
		.onLeaveCluster.register(tag, this.refresh.bind(this))
};
NGL.DevicesWidget.prototype = Object.assign(
	Object.create(NGL.BaseDimmolWidget.prototype),
	{ constructor: NGL.DevicesWidget }
);
NGL.DevicesWidget.prototype.setup = function (forPanel, stage, preferences) {
	if (!stage.dimmol.clusterManager.connected ||
		stage.dimmol.clusterManager.clusterConn.nodeType ===
			LIVVCLIB.NodeType.SLAVE) {
		return forPanel.add(new UI.Text('Only master nodes are allowed to connect to devices.'));
	}

	const $this = this;

	const textName = new UI.Input();
	const textAddress = new UI.Input(window.location.origin);
	const buttonConnect = new UI.Button('Connect').onClick(connect);

	[
		['Device name', textServer], ['Device address', textRoom]
	].forEach(function (element) {
		forPanel.add(
			new UI.Text(element[0]).setWidth('120px'),
			element[1],
			new UI.Break()
		);
	});
	forPanel.add(buttonConnect, new UI.Break());

	for (var key in stage.dimmol.devicesControl.devices) {
		addDevice(stage.dimmol.devicesControl.devices[key]);
	}

	function connect () {
		stage.dimmol.devicesControl
			.connectToDeviceInterface(
				textName.getValue(),
				textAddress.getValue(),
				LIVVCLIB.DeviceInterface.ANALOG
			)
			.then(function (device) {
				addDevice(device);
				textName.setValue('');
			})
			.catch(function (e) {
                alert(`Failed to connect to device.\n"${getErrorMessage(e)}"`);
			});
	}

	function disconnect (device) {
		return function () {
			stage.dimmol.devicesControl.disconnectDevice(device);
			$this.refresh();
		}
	}

	function addDevice (device) {
		forPanel.add(
			new UI.Text(device.name + '@' + device.address),
			new UI.Button('Disconnect').onClick(disconnect(device)),
			new UI.Break()
		);
	}
};

NGL.DimmolStructureComponentWidget = function (component, stage) {
	const signals = component.signals
	const container = new UI.CollapsibleIconPanel('minus-square', 'plus-square')
	const reprContainer = new UI.Panel()

	signals.representationAdded.add(function (repr) {
		reprContainer.add(
			new NGL.RepresentationComponentWidget(repr, stage)
		)
	})
		// Selection

	container.add(
		new UI.SelectionPanel(component.selection)
			.setMarginLeft('20px')
			.setInputWidth('214px')
	)

		// Export PDB

	var pdb = new UI.Button('export').onClick(function () {
		var pdbWriter = new NGL.PdbWriter(component.structure)
		pdbWriter.download('structure')
		componentPanel.setMenuDisplay('none')
	})

		// Add representation

	var repr = new UI.Select()
		.setColor('#444')
		.setOptions((function () {
			var reprOptions = { '': '[ add ]' }
			NGL.RepresentationRegistry.names.forEach(function (key) {
				reprOptions[ key ] = key
			})
			return reprOptions
		})())
		.onChange(function () {
			stage.dimmol.addRepresentation(component, repr.getValue());
			repr.setValue('')
			componentPanel.setMenuDisplay('none')
		})

	// Principal axes

	var alignAxes = new UI.Button('align').onClick(function () {
		var pa = component.structure.getPrincipalAxes()
		var q = pa.getRotationQuaternion()
		q.multiply(component.quaternion.clone().inverse())
		stage.animationControls.rotate(q)
		stage.animationControls.move(component.getCenter())
	})

	// Annotations visibility

	var showAnnotations = new UI.Button('show').onClick(function () {
		component.annotationList.forEach(function (annotation) {
			stage.dimmol.setAnnotationsVisible(component, true);
		})
	})

	var hideAnnotations = new UI.Button('hide').onClick(function () {
		component.annotationList.forEach(function (annotation) {
			stage.dimmol.setAnnotationsVisible(component, false);
		})
	})

	var annotationButtons = new UI.Panel()
		.setDisplay('inline-block')
		.add(showAnnotations, hideAnnotations)

	// Position

	var position = new UI.Vector3()
		.onChange(function () {
			component.setPosition(position.getValue())
		})

	// Rotation

	var q = new NGL.Quaternion()
	var e = new NGL.Euler()
	var rotation = new UI.Vector3()
		.setRange(-6.28, 6.28)
		.onChange(function () {
			e.setFromVector3(rotation.getValue())
			q.setFromEuler(e)
			component.setRotation(q)
		})

		// Scale

	var scale = new UI.Number(1)
		.setRange(0.01, 100)
		.onChange(function () {
			component.setScale(scale.getValue())
		})

		// Matrix

	signals.matrixChanged.add(function () {
		position.setValue(component.position)
		rotation.setValue(e.setFromQuaternion(component.quaternion))
		scale.setValue(component.scale.x)
	})

		// Component panel

	var componentPanel = new UI.ComponentPanel(component)
		.setDisplay('inline-block')
		.setMargin('0px')
		.addMenuEntry('PDB file', pdb)
		.addMenuEntry('Representation', repr)
		.addMenuEntry(
			'File',
			new UI.Text(component.structure.path)
				.setMaxWidth('100px')
				.setOverflow('auto')
				// .setWordWrap( "break-word" )
		)
		.addMenuEntry('Principal axes', alignAxes)
		.addMenuEntry('Annotations', annotationButtons)
		.addMenuEntry('Position', position)
		.addMenuEntry('Rotation', rotation)
		.addMenuEntry('Scale', scale)

	// Fill container

	container
		.addStatic(componentPanel)
		.add(reprContainer)

	return container
}