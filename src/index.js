// window.NGL = require('../lib/ngl.js');
window.LIVVCLIB = require('../lib/livvclib.client.min');

window.MoleculeControl = require('./MoleculeControl');
window.ClusterManager = require('./ClusterManager');

window.makeCameraDistributed = require('./makeCameraDistributed');
window.doOnPlatform = require('./doOnPlatform');
window.doOnMobile = require('./doOnMobile');
window.doOnDesktop = require('./doOnDesktop');
