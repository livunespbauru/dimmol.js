const LIVVCLIB = require('../lib/livvclib.client.min.js');

function newReprName (name) {
	return 'dimmol:repr:' + name;
}

/**
 * @author luizssb
 */
function RepresentationControl (repr, uniqId, clusterManager) {
	this.repr = repr;
	this.clusterConn = clusterManager.clusterConn;
	this.sync =
		new LIVVCLIB.Sync(clusterManager.clusterConn, newReprName(uniqId));
	this._registerTrackings(repr, this.sync);
}

const Prototype = RepresentationControl.prototype;

Prototype._registerTrackings = function (repr, sync) {
	sync
		.trackMethod(repr, 'setParameters', function (obj, method, args) {
			obj[method].apply(obj, args);
			repr.stage.viewer.requestRender();
		})
		.trackMethod(repr, 'setVisibility');
};

Prototype.dispose = function () {
	this.sync.stopAllTracking();
	this.sync = null;
};

module.exports = RepresentationControl;
