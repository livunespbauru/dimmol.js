const doOnPlatform = require('./doOnPlatform');

module.exports = function (onMobile) {
	doOnPlatform(null, onMobile);
};
