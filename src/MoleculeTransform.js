const LIVVCLIB = require('../lib/livvclib.client.min');

function MoleculeTransform () {
	this.updateRateMs = 16;
}

const Prototype = MoleculeTransform.prototype;

LIVVCLIB.ObjectEx.defineProperties(
	Prototype,
	{
		autoRotation: {
			get: function () { return this._autoRotation; },
			set: function (value) {
				this._autoRotation = value;
				this.update();
			}
		},
		autoTranslation: {
			get: function () { return this._autoTranslation; },
			set: function (value) {
				this._autoTranslation = value;
				this.update();
			}
		}
	}
);

Prototype.set = function (position, rotation) {
	this._autoRotation = rotation;
	this._autoTranslation = position;
	this.update();
};

Prototype.hasUpdate = function () {
	const props = [this.autoRotation, this.autoTranslation];

	for (var idx in props) {
		for (var subProp in props[idx]) {
			if (props[idx][subProp] !== 0) {
				return true;
			}
		}
	}

	return false;
};

Prototype.update = function () {
	if (this.hasUpdate()) {
		if (!this._key) {
			this.autoTranslation.divideScalar(this.updateRateMs);
			this.autoRotation.x /= this.updateRateMs;
			this.autoRotation.y /= this.updateRateMs;
			this.autoRotation.z /= this.updateRateMs;

			this._update();
			this._key = setInterval(this._update.bind(this), this.updateRateMs);
		}
	} else {
		clearInterval(this._key);
		this._key = null;
		this._update();
	}
};

Prototype._update = function () {
	if (!this.component) {
		return;
	}

	this.component.autoTranslation.add(this.autoTranslation);

	var rotation =
		new THREE.Euler().setFromQuaternion(this.component.quaternion).toArray();
	rotation.splice(3);
	rotation[0] += this.autoRotation.x / this.updateRateMs;
	rotation[1] += this.autoRotation.y;
	rotation[2] += this.autoRotation.z / this.updateRateMs;
	this.component.setRotation(rotation);

	this.component.updateMatrix();
};

Prototype.moveToCenter = function () {
	if (this.component) {
		this.component.setPosition([0, 0, 0]);
	}
};

module.exports = MoleculeTransform;
