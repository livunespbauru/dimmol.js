const LIVVCLIB = require('../lib/livvclib.client.min');
const doOnMobile = require('../lib/doOnMobile');
const DeviceSupport = require('../lib/DeviceSupport');

function ConnectedInterface (access, tracker) {
	this.access = access;
	this.tracker = tracker;

	this.name = access.deviceName;
	this.address = access.deviceAddress;
};

function makeDeviceId (name, address) {
	return [name, address, LIVVCLIB.DeviceInterface.ANALOG].join(':');
}

function DevicesControl (clusterManager) {
	this.tag = LIVVCLIB.RandomUtils.string();
	this._clusterManager = clusterManager;
	this.moleculeControl = null;
	this._devices = {};
}

const Prototype = DevicesControl.prototype;

LIVVCLIB.ObjectEx.defineProperties(
	Prototype,
	{
		devices: LIVVCLIB.ObjectEx.readOnly(
			function () { return this._devices; }
		)
	}
);

Prototype.setUp = function () {
	doOnMobile(function () {
		this._deviceListener =
			new LIVVCLIB.NewDevicesListener(this.clusterManager.serverConn)
				.onNewDevice.register(
					this.tag,
					function (sender, access, tracker) {
						// _deviceAccess = access;
						// _analog = registerAnalogChange(tracker);
					})
				.startListening();
	}.bind(this));
};

Prototype.connectToDeviceInterface = function (
	name, address// deviceInterface
) {
	const deviceInterface = DeviceSupport.Interface.ANALOG;

	if (!DeviceSupport.supportsDevice(name, address, deviceInterface)) {
		throw new Error('device not supported');
	}

	return new Promise(function (resolve, reject) {
		this._clusterManager.doOnNode(
			function () { // Master
				const deviceId = makeDeviceId(name, address);

				const deviceAccess = new LIVVCLIB.DeviceControlAccess(
					this._clusterManager.serverConn,
					name, address, deviceInterface
				);

				const tracker = new LIVVCLIB.AnalogTracker(deviceAccess)
					.getOnDeviceEvent(LIVVCLIB.AnalogEvents.CHANGE)
					.register(this.tag, this._handleDeviceData.bind(this));

				deviceAccess
					.onConnect.register('', function (sender, error) {
						tracker.subscribeToInterfaceEvent(
							LIVVCLIB.AnalogEvents.CHANGE, false
						);
						return false;
					})
					.onConnectError.register('', function (sender, error) {
						reject(error);
						return false;
					})
					.onSubscribe.register('', function (sender) {
						this._devices[deviceId] =
							new ConnectedInterface(deviceAccess, tracker);
						resolve(this._devices[deviceId]);
						return false;
					}.bind(this))
					.onSubscribeError.register('', function (sender, error) {
						reject(error);
						return false;
					})
					.connect();
			},
			function () { // Slave
				reject(new Error('cannot connect to device from slave node'));
			}
		);
	}.bind(this));
};

Prototype._handleDeviceData = function (sender, channels) {
	if (!this.moleculeControl) {
		return;
	}

	const rotation = new THREE.Euler(channels[1], channels[0]);
	const position = new THREE.Vector3(-channels[2], channels[3]);

	if (channels[4] !== -1) {
		switch (channels[4]) {
		case 0: position.z = -1; break;
		case 45:
			position.z = -1;
			rotation.z = 1;
			break;
		case 90: rotation.z = 1; break;
		case 135:
			rotation.z = 1;
			position.z = 1;
			break;
		case 180: position.z = 1; break;
		case 225:
			position.z = 1;
			rotation.z = -1;
			break;
		case 270: rotation.z = -1; break;
		case 315:
			rotation.z = -1;
			position.z = -1;
			break;
		}
	}

	this.moleculeControl.transform.set(position, rotation);
};

module.exports = DevicesControl;
