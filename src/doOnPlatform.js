module.exports = function (onDesktop, onMobile) {
	if (/Android|webOS|iPhone|iPad|iPod|Opera Mini/i.test(navigator.userAgent)) {
		if (onMobile) {
			onMobile();
		}
	} else {
		if (onDesktop) {
			onDesktop();
		}
	}
};
