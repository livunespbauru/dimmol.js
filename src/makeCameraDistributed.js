const LIVVCLIB = require('../lib/livvclib.client.min');

module.exports = function (camera) {
	var CameraPrototype = Object.getPrototypeOf(camera);
	CameraPrototype.projectionPlane = null;
	CameraPrototype.screenPoints = null;
	CameraPrototype.updateProjectionMatrix_threejs = CameraPrototype.updateProjectionMatrix;
	CameraPrototype.calculateProjectionMatrix =
		LIVVCLIB.DistributedCamera.prototype.calculateProjectionMatrix;
	CameraPrototype.updateProjectionMatrix =
		LIVVCLIB.DistributedCamera.prototype.updateProjectionMatrix;
	CameraPrototype.attachToCluster =
		LIVVCLIB.DistributedCamera.prototype.attachToCluster;
	CameraPrototype.detachFromCluster =
		LIVVCLIB.DistributedCamera.prototype.detachFromCluster;

	return camera;
};
