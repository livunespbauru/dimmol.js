const doOnPlatform = require('./doOnPlatform');

module.exports = function (onDesktop) {
	doOnPlatform(onDesktop);
};
