const LIVVCLIB = require('../lib/livvclib.client.min');
const doOnMobile = require('./doOnMobile');

function getConnectionFactory () {
	const nodeFactory = new LIVVCLIB.NodeConnectionFactory();

	doOnMobile(function () {
		nodeFactory.preferredConnectionMethod =
			LIVVCLIB.NodeConnectionMethod.SERVER;
	});

	return nodeFactory;
}

function ClusterManager () {
	this.onLeaveCluster = new LIVVCLIB.ObjectEvent(this);
	this.onJoinCluster = new LIVVCLIB.ObjectEvent(this);
}

const Prototype = ClusterManager.prototype;

LIVVCLIB.ObjectEx.defineProperties(
	Prototype,
	{
		connected: LIVVCLIB.ObjectEx.readOnly(function () {
			return this.clusterConn && this.clusterConn.joined;
		})
	});

Prototype.connect = function (serverAddress, room) {
	if (this.serverConn &&
		(this.serverConn.serverAddress !== serverAddress ||
			this.serverConn.room !== room)) {
		return new Promise(function (resolve, reject) {
			reject(new Error('already connected to another cluster'));
		});
	}

	if (this._promiseConnect) {
		return this._promiseConnect;
	}

	this.serverConn =
		new LIVVCLIB.SocketIOServerConnection(serverAddress);
	this.clusterConn = new LIVVCLIB.MasterSlaveCluster(
		this.serverConn, room, getConnectionFactory()
	);

	this._promiseConnect = new Promise(function (resolve, reject) {
		const key = LIVVCLIB.RandomUtils.string();

		this.clusterConn
			.onJoinCluster.register(key, function (sender, participants) {
				this.eventManager =
					new LIVVCLIB.ClusterDataEventManager(this.clusterConn)
						.start();
				this.onJoinCluster.invoke();
				resolve(participants);
				return false;
			}.bind(this))
			.onJoinClusterError.register(key, function (sender, error) {
				reject(error);
				this.disconnect();
				return false;
			})
			.onLeaveCluster.register(key, function (sender) {
				this.onLeaveCluster.invoke();
				this.disconnect();
				return false;
			}.bind(this))
			.joinCluster();
	}.bind(this));

	return this._promiseConnect;
};

Prototype.disconnect = function () {
	if (this.connected) {
		this.serverConn.disconnect();
		delete this.serverConn;
		delete this.clusterConn;
		delete this._promiseConnect;
	}
};

Prototype.doOnMasterNode = function (onMaster) {
	return this.doOnNode(onMaster, null);
};

Prototype.doOnSlaveNode = function (onSlave) {
	return this.doOnNode(null, onSlave);
};

Prototype.doOnNode = function (onMaster, onSlave) {
	if (this.clusterConn.nodeType === undefined) {
		throw new Error('can\'t perform node action before connecting');
	}

	if (this.clusterConn.nodeType === LIVVCLIB.NodeType.MASTER) {
		if (onMaster) {
			return onMaster();
		}
	} else {
		if (onSlave) {
			return onSlave();
		}
	}
};

Prototype.onLeaveCluster = null;

Prototype.onJoinCluster = null;

module.exports = ClusterManager;
