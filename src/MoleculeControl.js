const LIVVCLIB = require('../lib/livvclib.client.min.js');

const MoleculeTransform = require('./MoleculeTransform');
const RepresentationControl = require('./RepresentationControl');

const KEY_SYNC = 'dimmol:moleculeControl:component';
const DEFAULT_REPRESENTATION = 'hyperball';


function reprUniqId () {
	this.idx = this.idx | 0;
	return this.idx;
}

/**
 * @author luizssb
 */
function MoleculeControl (stage, clusterManager) {
	this.stage = stage;
	this.clusterManager = clusterManager;
	this.transform = new MoleculeTransform();
	this.reprs = {};
}

const Prototype = MoleculeControl.prototype = {
	component: null
};

Prototype._registerTrackings = function (component, sync) {
	sync
		.trackMethod(component, 'setPosition')
		.trackMethod(
			component, 'setRotation',
			function (obj, method, args) {
				obj[method](Object.values(args[0]));
			}
		)
		.trackMethod(component, 'setScale')
		.trackMethod(component, 'setVisibility')
		.trackMethod(component, 'autoView');
};

Prototype.load = function (filePath) {
	this.unload();

	return this.stage.loadFile(filePath, {defaultRepresentation: true})
		.then(function (component) {
			this.component = component;
			this.sync = new LIVVCLIB.Sync(
				this.clusterManager.clusterConn, KEY_SYNC
			);
			this.transform.component = component;

			this._registerTrackings(component, this.sync);
			this.stage.autoView();

			const defaultRepr = component.reprList[0];
			this.reprs[defaultRepr.name] = new RepresentationControl(
				defaultRepr, reprUniqId(), this.clusterManager
			);
		}.bind(this)
	);
};

Prototype.removeComponent = function (component) {
	this.removeAllRepresentations();
	return this.stage.removeComponent(component);
};

Prototype.removeAllComponents = function () {
	return this.removeComponent(this.component);
};

Prototype.setAnnotationsVisible = function (component, visible) {
	return this.component.annotationList.forEach(function (annotation) {
		annotation.setVisibility(visible);
	});
};

Prototype.addRepresentation = function (toComponent, reprName, params) {
	if (!this.reprs[reprName]) {
		const repr = toComponent.addRepresentation(reprName, params);

		this.reprs[reprName] = new RepresentationControl(
			repr,
			reprUniqId(),
			this.clusterManager
		);

		return repr;
	}
};

Prototype.removeRepresentation = function (representation) {
	this.reprs[representation.name].dispose();
	delete this.reprs[representation.name];
	return this.component.removeRepresentation(representation);
};

Prototype.removeAllRepresentations = function () {
	for (var reprName in this.reprs) {
		this.reprs[reprName].dispose();
		delete this.reprs[reprName];
	}

	return this.component.removeAllRepresentations();
};

Prototype.unload = function () {
	if (this.component) {
		this.sync.stopAllTracking();
		this.sync = null;
		this.stage.removeComponent(this.component);
		this.component = null;
	}
};

module.exports = MoleculeControl;
