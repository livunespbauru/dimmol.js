const LIVVCLIB = require('../lib/livvclib.client.min');

module.exports = {
	Interface: {
		ANALOG: LIVVCLIB.DeviceInterface.ANALOG
	},
	DeviceType: {
		XINPUT: 'xinput'
	},
	supportsDevice: function (name, address, deviceInterface) {
		name = name.toLowerCase();

		let deviceSupport = false;
		for (let type in this.DeviceType) {
			if (name.indexOf(this.DeviceType[type]) > -1) {
				deviceSupport = true;
				break;
			}
		}

		let interfaceSupport = false;
		for (let type in this.Interface) {
			if (deviceInterface.indexOf(this.Interface[type]) > -1) {
				interfaceSupport = true;
				break;
			}
		}

		return deviceSupport && interfaceSupport;
	}
};
